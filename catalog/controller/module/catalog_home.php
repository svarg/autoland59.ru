<?php
class ControllerModuleCatalogHome extends Controller {
	public function index() {

		$data['heading_title'] = $this->language->get('heading_title');
		$this->load->model('catalog/product'); //подключаем любую модель из OpenCart

		$data['products']=$this->model_catalog_product->getProducts();
		shuffle($data['products']);
		array_splice($data['products'],9);

		for ($i=0; $i<count($data['products']);$i++){
			$data['products'][$i]['patch'] = $this->model_catalog_product->getCategories($data['products'][$i]['product_id'])[0]['category_id'];
		}


		
		return $this->load->view('autoland/template/module/catalog_home.tpl', $data);


	}

}