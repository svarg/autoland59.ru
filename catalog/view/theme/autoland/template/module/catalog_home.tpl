<div class="row catalog-home">
<div class="col-ms-12">
    <h1>Каталог</h1>
</div>
<?php
foreach($products as $product){ ?>
        <div class="col-sm-4">
            <div class="product">
                <div class="item-image">
                    <a href="/index.php?route=product/product&path=<?=$product['patch']?>&product_id=<?=$product['product_id']?>">
                    <?php
                        if (file_exists('/image'.$product['image'])){
                    ?>
                    <img src="<?= $product['image'] ?>">
                    <?php
                        }
                        else{
                    ?>
                    <img src="/image/no_image.png">
                    <?php } ?>
                    </a>
                </div>

                <div class="item-name">
                    <a href="/index.php?route=product/product&path=<?=$product['patch']?>&product_id=<?=$product['product_id']?>">
                    <?= $product['name'] ?>
                    </a>
                </div>
                <div  class="item-price">
                    <?= substr($product['price'],0,-5) ?> р.
                </div>

                <div class="item-button">
                    <button type="button" onclick="cart.add('<?=$product['product_id']?>', '1');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">В корзину</span></button>
                </div>
            </div>
        </div>
<?php
}
?>
</div>
